var obj = {
    "avgRatings": 4.2,
    "ratingsCount": 56,
    "review": "1,117",
    "rating": "16,841",

    "ratingDetails": [{
        "text": "5",
        "avgRatingsPercentage": 70
    },
    {
        "text": "4",
        "avgRatingsPercentage": 13
    },
    {
        "text": "3",
        "avgRatingsPercentage": 5
    },
    {
        "text": "2",
        "avgRatingsPercentage": 2
    }, {
        "text": "1",
        "avgRatingsPercentage": 10
    }


    ]
};
var script = document.createElement('script');
script.src = "https://kit.fontawesome.com/9d091bc651.js";
document.head.appendChild(script);


var rating = document.getElementById("rating");
rating.style.cssText = " display: flex;align-items: center;justify-content: center;flex-wrap: wrap;width: 350PX;"


// creating 4.4 rating div //
function totalrating(heading, fontsize, ratingnum) {
    var totalrating = document.createElement("div");
    totalrating.style.cssText = "display: flex;justify-content: center;align-items: center;background:#378E3C;color: white;border-radius: 5px;width: 60px;padding: 3px 0;"
    rating.appendChild(totalrating);

    var headingtag = document.createElement(heading);
    headingtag.style.cssText = "margin: 0 5px;"
    headingtag.innerHTML = ratingnum; // total rating
    totalrating.appendChild(headingtag);

    var span = document.createElement("span");
    span.className = "fas fa-star";
    span.style.cssText = `font-size:${fontsize}`;
    totalrating.appendChild(span);
}
totalrating("h3", "12px", obj.avgRatings);


// creating reviews div //

function reviews(heading, ratingval, reviewval) {
    var reviews = document.createElement("div");
    rating.appendChild(reviews);

    var reviewh3 = document.createElement(heading);
    reviewh3.innerHTML = `${ratingval} Ratings & ${reviewval} Reviews`;
    reviewh3.style.cssText = "margin: 0 5px;color:#868687"
    reviews.appendChild(reviewh3)
}
reviews("h3", obj.rating, obj.review);